package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/golang-collections/collections/stack"
	"log"
	"os"
	"os/exec"
	"runtime"
)


var fileName string
var outPath string

func main() {
	setupArgs()
	asmFile := compile(fileName)
	objFile := assemble(asmFile)
	link(objFile)
}

// Compiles brainfuck into x86 instructions and writes them into a temp .asm file and returns the filename
func compile(filePath string) string {
	labels := stack.New()
	currentLabel := 0

	bfString := readSanitizeFile(filePath)
	outAsm := `  global _start
  extern getchar
  extern putchar
  extern exit
  section .text
_start:
  sub rsp, 4000
  mov eax, 0
  mov ecx, 4000
  mov rdi, rsp
  rep stosb
  mov r12, rsp
  sub rsp, 64
`
	for _, char := range bfString {
		switch char {
		case '<':
			outAsm += "  add r12, 1\n"
			break
		case '>':
			outAsm += "  sub r12, 1\n"
			break
		case '+':
			outAsm += "  add byte [r12], 1\n"
			break
		case '-':
			outAsm += "  sub byte [r12], 1\n"
			break
		case ',':
			outAsm += "  call getchar\n"
			outAsm += "  mov byte [r12], al\n"
			break
		case '.':
			if runtime.GOOS == "windows" {
				outAsm += "  mov cl, [r12]\n"
			} else {
				outAsm += "  mov dil, [r12]\n"
			}
			outAsm += "  call putchar\n"
			break
		case '[':
			outAsm += fmt.Sprintf("label%d:\n", currentLabel)
			outAsm += "  cmp byte [r12], 0\n"
			outAsm += fmt.Sprintf("  jz label%dend\n", currentLabel)
			labels.Push(currentLabel)
			currentLabel++
			break
		case ']':
			outAsm += fmt.Sprintf("  jmp label%d\n", labels.Peek())
			outAsm += fmt.Sprintf("label%dend:\n", labels.Peek())
			labels.Pop()
			break
		default:
			break
		}
	}
	return writeAssemblyFile(outAsm)
}

// Assembles generated assembly into an object file, and returns the filename
func assemble(asmPath string) string {
	format := "win64"
	outFile := "tmp.obj"
	if runtime.GOOS != "windows" {
		format = "elf64"
		outFile = "tmp.o"
	}
	cmd := exec.Command("nasm", "-f", format, asmPath, "-o", outFile)
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	return outFile
}

// Links object file with OS provided C library and returns executable filename
func link(objPath string) string {
	outFile := "bf.exe"
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		linker := os.Getenv("LINK64")
		ucrt := os.Getenv("UCRT64")
		outParam := fmt.Sprintf("/OUT:%s", outFile)

		cmd = exec.Command(linker, "/SUBSYSTEM:CONSOLE", "/ENTRY:_start", objPath, outParam, ucrt)
	} else {
		outFile = "bf"
		cmd = exec.Command("ld", "-lc", objPath, "-o", outFile, "-I", "/lib64/ld-linux-x86-64.so.2")
	}
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	return outFile
}

// Writes generated assembly into "tmp.asm" file, overwrites any existing data
func writeAssemblyFile(outAsm string) string {
	file, err := os.Create("tmp.asm")
	if err != nil {
		file2, err2 := os.OpenFile("tmp.asm", os.O_RDWR, 0644)
		check(err2)
		file = file2
	}
	defer file.Close()
	_, _ = fmt.Fprintf(file, outAsm)
	return "tmp.asm"
}

// Sets up command line flags and parses them into global variables.
func setupArgs()  {
	flag.StringVar(&fileName, "f", "", "Path to brainfuck file to compile.")
	flag.StringVar(&outPath, "o", "", "Path to output executable.")
	flag.Parse()
}

// Reads Brainfuck file provided, removes all whitespace and returns it as a string
func readSanitizeFile(filePath string) string {
	file, err := os.Open(filePath)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)
	output := ""
	for scanner.Scan() {
		output += scanner.Text()
	}
	return output
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
